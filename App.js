import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState}from 'react';
import { StyleSheet, Text, View, Pressable } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faUser,
    faMartiniGlassCitrus,
    faHome,
    faList,
    faSearch,
    faPerson,
  } from "@fortawesome/free-solid-svg-icons";
import StepOne from './components/StepOne';
import StepTwo from './components/StepTwo';
import StepThree from './components/StepThree';
import StepFive from './components/StepFive';
const Stack = createNativeStackNavigator();
export default function App() {

/* step 6 */
const [items, setItems] = useState([]);
const addItem = (value) =>
{
  //check if value exist in array
  if(items.indexOf(value) > -1 !=true)
  {
    setItems([...items, value]);
  }
  else
  {
    items.splice(items.indexOf(value), 1);
    setItems([...items]);
  }
  console.log('items in array', items)
}
function StepOneScreen({ navigation }) {
  return (
    <View style={styles.container}>
        <StepOne items={items} addItem={addItem}/>
        <BottomMenu></BottomMenu>
    </View>
  );
}
function StepTwoScreen({ navigation }) {
  return (
    <View style={styles.container}>
        <StepTwo items={items} addItem={addItem}/>
        <BottomMenu></BottomMenu>
    </View>
  );
}
function StepThreeScreen({ navigation }) {
  return (
    <View style={styles.container}>
        <StepThree items={items} addItem={addItem}/>
        <BottomMenu></BottomMenu>
    </View>
  );
}
function StepFiveScreen({ route, navigation }) {
  const { itemId } = route.params;
  return (
    <View style={styles.container}>
        <StepFive itemId={itemId} items={items} addItem={addItem}/>
        <BottomMenu></BottomMenu>
    </View>
  );
}
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="StepOne" screenOptions={{
        headerStyle: {
          backgroundColor: '#16A085',
        },
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
        <Stack.Screen name="StepOne" component={StepOneScreen} options={{title:'Step 1'}}/>
        <Stack.Screen name="StepTwo" component={StepTwoScreen} options={{title:'Step 2'}} />
        <Stack.Screen name="StepThree" component={StepThreeScreen} options={{title:'Step 3'}} />
        <Stack.Screen name="StepFive" component={StepFiveScreen} options={{title:'Step 5'}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

/* step 4 */
const BottomMenu = () =>
{
  const navigation = useNavigation();
  return (
  <View style={styles.bottomMenu}>
      <Pressable onPress={() => navigation.navigate('StepOne')} style={styles.button}>
        <FontAwesomeIcon icon={faHome} size={25}/>
      </Pressable>
      <Pressable onPress={() => navigation.navigate('StepTwo')} style={styles.button}>
        <FontAwesomeIcon icon={faSearch} size={25}/>
      </Pressable>

      <Pressable onPress={() => navigation.navigate('StepThree')} style={styles.button}>
        <FontAwesomeIcon icon={faPerson} size={25}/>
      </Pressable>
      
  </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  bottomMenu: {
    flexDirection:'row',
    backgroundColor:"#16A085",
    justifyContent:"space-between",
    padding: 5,
    paddingLeft: 40,
    paddingRight:40,
    borderTopColor:"#029aaa",
    borderTopWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }
});
