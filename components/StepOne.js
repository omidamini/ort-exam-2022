import {StyleSheet,Dimensions,Image,TouchableHighlight, View,ScrollView, Pressable, Text } from 'react-native'
import React, {useEffect, useState}from 'react';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faMartiniGlass,
    faLayerGroup,
    faWineGlass,
    faArrowLeft,
    faPerson,
    faCircleArrowRight,
    faRotateRight,
  } from "@fortawesome/free-solid-svg-icons";

const StepOne = (props) => {
    console.log('props', props)
    const [data, setData] = useState({});
    const navigation = useNavigation();
    useEffect(() => 
    {
    (async () => 
    {
        fetchDataFromApi();
    })();
    }, [])
    const fetchDataFromApi = () => {
        fetch(`https://thesimpsonsquoteapi.glitch.me/quotes`)
        .then(response => response.json())
        .then(data => {
            //console.log(data)
            setData(data[0])
        })
        .catch((error) => {
        console.error(error)
        })
        //.finally(() => setData(false)); 
    }
    const CitationItem = ({CitationItem}) => 
    {
        console.log('img',CitationItem.image)
        
        return (
            <View  style={styles.item}>
                    
                     <TouchableHighlight style={styles.sub_item} onPress={() => navigation.navigate('StepFive', {itemId: CitationItem.character})}>
                        <Image source={{uri: CitationItem.image}} style={styles.image} />
                    </TouchableHighlight>
                    <View style={{flexDirection:'row', width:'100%', justifyContent:'center'}}>
                        
                        <View style={{flexDirection:'column', padding:6, textAlign:'center'}}>
                            <Text  style={styles.title}><FontAwesomeIcon icon={faPerson}/> {CitationItem.character}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', height:'80%', justifyContent:'center', marginTop:10}}>
                        <Text  style={styles.text}><Text style={{fontWeight:'400'}}>quote : </Text>{CitationItem.quote}</Text>
                        {
                            props.items && props.items.find(e => e == CitationItem.character)?
                            <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                                <FontAwesomeIcon icon={faHeart} size={40}  style={{color:'purple'}}/>
                            </Pressable>:
                            <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                                <FontAwesomeIcon icon={faHeart} size={40}  /><Text></Text>
                            </Pressable>
                        }
                    </View>
                    
                    
            </View>
        )
    }
  return (
    <ScrollView vertical={true} style={styles.scrollView}>
        <View  style={styles.ItemContainer}>
            <CitationItem CitationItem= {data}></CitationItem>
        </View>
        <View style={styles.BackBottom} horizontal={false}>
            <Pressable onPress={() => fetchDataFromApi()} style={styles.button}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faRotateRight} size={25}/></Text>
            </Pressable>
        </View>
    </ScrollView>
  )
  
}
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        marginTop: 20,
        paddingTop: 0,
        marginBottom: 20,
        paddingBottom: 20,
        backgroundColor: "#16A085",
        borderRadius: 10,
    },
    item: {
        height: deviceHeight -250,
        width: deviceWidth * 0.4,
        borderRadius: 10,
        margin:4
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        justifyContent:'center',
        alignItems:'center',
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: 300,
        width: 100
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },   
    button: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "#16A085"
    },
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#16A085",
        justifyContent:"center",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        marginTop:0,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
})
export default StepOne