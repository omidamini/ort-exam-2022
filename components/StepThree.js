import {StyleSheet,TextInput,TouchableHighlight, Dimensions,Image, View,ScrollView, Pressable, Text } from 'react-native'
import React, {useEffect, useState}from 'react';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faMartiniGlass,
    faLayerGroup,
    faWineGlass,
    faArrowLeft,
    faPerson,
    faSearch,
    faCircleArrowRight,
    faRotateRight,
  } from "@fortawesome/free-solid-svg-icons";

const StepThree = (props) => {
    const [data, setData] = useState({});
    const navigation = useNavigation();
    const [text, setText] = useState('');
    useEffect(() => 
    {
    (async () => 
    {
        fetchDataFromApi();
    })();
    }, [])
    const fetchDataFromApi = () => {
        if (text == '') {
            setData({});
        }
        else
        {
            fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?character=${text}`)
            .then(response => response.json())
            .then(data => {
                console.log(data)
                setData(data[0])
            })
            .catch((error) => {
            console.error(error)
            })
        }
        
    }
    const handleClick = () =>
    {
        fetchDataFromApi();
    }
    const CitationItem = ({CitationItem}) => 
    {
       
        return (
            CitationItem && CitationItem.character? 
            <View  style={styles.item}>
                <TouchableHighlight style={styles.sub_item} onPress={() => navigation.navigate('StepFive', {itemId: CitationItem.character})}>
                    <Image source={{uri: CitationItem.image}} style={styles.image} />
                </TouchableHighlight>
                <View style={{flexDirection:'row', width:'100%', justifyContent:'center'}}>
                    
                    <View style={{flexDirection:'column', padding:6, textAlign:'center'}}>
                        <Text  style={styles.title}><FontAwesomeIcon icon={faPerson}/> {CitationItem.character}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', height:'80%', justifyContent:'center', marginTop:10}}>
                    <Text  style={styles.text}><Text style={{fontWeight:'400'}}>quote : </Text>{CitationItem.quote}</Text>
                </View>
                {
                    props.items && props.items.find(e => e == CitationItem.character)?
                    <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                        <FontAwesomeIcon icon={faHeart} size={40}  style={{color:'purple'}}/>
                    </Pressable>:
                    <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                        <FontAwesomeIcon icon={faHeart} size={40}  /><Text></Text>
                    </Pressable>
                }
            </View> :
            <View/>
        )
    }
  return (
    console.log("data : ",data),
    <ScrollView vertical={true} style={styles.scrollView}>
        <View style={{flexDirection:'row', width:'90%', margin:10, padding:10,  justifyContent:'center', backgroundColor:'#16A085'}}>
            <TextInput
                style={styles.input}
                onChangeText={(text) => setText(text)}
                value = {text}
                placeholder="écrivez le nom d'un personnage"
                //keyboardType="numeric"
            />
            <Pressable style={styles.button} onPress={() => handleClick()}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faSearch} size={25}/></Text>
            </Pressable>
            
        </View>
        <View  style={styles.ItemContainer}>
            
            <CitationItem CitationItem= {data}></CitationItem>
        </View>
    </ScrollView>
  )
  
}
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        marginTop: 20,
        paddingTop: 0,
        marginBottom: 20,
        paddingBottom: 20,
        backgroundColor: "#16A085",
        borderRadius: 10,
    },
    item: {
        height: deviceHeight -250,
        width: deviceWidth * 0.4,
        borderRadius: 10,
        margin:4
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        justifyContent:'center',
        alignItems:'center',
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: 300,
        width: 100
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },   
    input: {
        height: 40,
        margin: 0,
        borderWidth: 1,
        padding: 10,
        borderColor:'#000'
      },
      button: {
        
        height: 40,
        margin: 0,
        borderWidth: 1,
        padding: 10,
        borderColor:'#000'
      },
      
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#16A085",
        justifyContent:"center",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        marginTop:0,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
})
export default StepThree