import {StyleSheet,TouchableHighlight,Dimensions,Image,TextInput, View,ScrollView, Pressable, Text } from 'react-native'
import React, {useEffect, useState}from 'react';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faMartiniGlass,
    faLayerGroup,
    faWineGlass,
    faArrowLeft,
    faPerson,
    faCircleArrowRight,
    faRotateRight,
    faSearch,
  } from "@fortawesome/free-solid-svg-icons";

const StepTwo = (props) => {
    const [data, setData] = useState({});
    const [text, setText] = useState(null);
    const navigation = useNavigation();
    useEffect(() => 
    {
        (async () => 
        {
            fetchDataFromApi();
        })();
    }, [])
    const fetchDataFromApi = () => {
        fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${text}`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            setData(data)
        })
        .catch((error) => {
        console.error(error)
        })
        //.finally(() => setData(false)); 
    }
    const handleClick = () =>
    {
        fetchDataFromApi();
    }
    const CitationListItem = ({CitationItem}) => {
        const navigation = useNavigation();
        return (
            <View  style={styles.item} >
                
                <TouchableHighlight style={styles.sub_item} onPress={() => navigation.navigate('StepFive', {itemId: CitationItem.character})}>
                    <Image source={{uri: CitationItem.image}} style={styles.image} />
                </TouchableHighlight>
                <Text  style={styles.title}><FontAwesomeIcon icon={faPerson}/> {CitationItem.character}</Text>
                {
                    props.items && props.items.find(e => e == CitationItem.character)?
                    <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  style={{color:'purple'}}/>
                    </Pressable>:
                    <Pressable style={styles.button} onPress={() => props.addItem(CitationItem.character)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  /><Text></Text>
                    </Pressable>
                }
            </View>
        )
    }
  return (
    
    <ScrollView vertical={true} style={styles.scrollView}>
        <View style={{flexDirection:'row', width:'90%', margin:10, padding:10,  justifyContent:'center', backgroundColor:'#16A085'}}>
            <TextInput
                style={styles.input}
                onChangeText={(text) => setText(text)}
                value = {text}
                placeholder="écrivez No° citation"
                //keyboardType="numeric"
            />
            <Pressable style={styles.button} onPress={() => handleClick()}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faSearch} size={25}/></Text>
            </Pressable>

            
        </View>
        <View  style={styles.ItemContainer} horizontal={false}>
            {
                data && data.length > 0 ? 

                data.map((data, idx) => (
                    console.log("data : ",data , 'with id :', idx),

                    <CitationListItem key={idx} CitationItem={data}/>
                ))

                :
                <View/>
            }
        </View>
    </ScrollView>
)
  
}
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 20
    },
    item: {
        height: deviceWidth * 0.6,
        width: deviceWidth * 0.4,
        backgroundColor: "#16A085",
        borderRadius: 10,
        margin:4
    },
    sub_item: {
        alignSelf:"center",
        height: "75%",
        width: "50%",
        backgroundColor:'#16A085',
        borderRadius: 10,
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        flex: 1,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: undefined,
        width: undefined
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    }, 
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#16A085",
        justifyContent:"center",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        marginTop:0,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      },
      input: {
        height: 40,
        margin: 0,
        borderWidth: 1,
        padding: 10,
        borderColor:'#000'
      },
      button: {
        
        height: 40,
        margin: 0,
        borderWidth: 1,
        padding: 10,
        borderColor:'#000'
      },
      
})
export default StepTwo